'use strict';
const Path = require('fire-path');
const Fs = require('fire-fs');
const {android, ios} = Editor.require('app://editor/core/native-packer');
/**
 * 添加 facebook audience network 的 sdk 到 android 工程
 * @param options
 * @returns {Promise}
 */
async function _handleAndroid(options) {
    Editor.log('Google Play--> adding Google Play Billing Android support');

    //修改build.gradle文件
    let androidPacker = new android(options);
    if (!androidPacker.addDependence('com.android.billingclient:billing', '2.0.0')) {
        return Promise.reject();
    }

    //拷贝android文件
    let srcAndroidPath = Editor.url('packages://googleplaybiling/libs/android');
    let destAndroidPath = Path.join(options.dest, 'frameworks/runtime-src/proj.android-studio/app/src/org/cocos2dx/javascript');

    let fileList = ['GPBillingHelper.java'];
    fileList.forEach((file) => {
        androidPacker.ensureFile(Path.join(srcAndroidPath, file), Path.join(destAndroidPath, file));
    });

    _copyFsupportFile(options, androidPacker);
}

/**
 * android 和 iOS 共用的资源拷贝
 * @param options
 * @param packer
 * @private
 */
function _copyFsupportFile(options, packer) {
    //拷贝脚本文件
    Editor.log('Google Play Billing--> _copyFsupportFile start');
    let srcJsPath = Editor.url('packages://googleplaybiling/libs/js');
    let destJsPath = Path.join(options.dest, 'src');
    Fs.copySync(srcJsPath, destJsPath);

    //在main.js中添加引用
    packer.addRequireToMainJs("src/ccGPBilling.js");
    Editor.log('Admob-->  _copyFsupportFile end');
}


async function handleEvent(options, cb) {
    // let config = Editor._projectProfile.data['admob'];
    // let data = JSON.stringify(Editor._projectProfile);
    // Editor.log("this is admob config:"+ data);
    // if (!config || !config.enable) {
    //     cb && cb();
    //     return;
    // }

    try {
        if (options.actualPlatform.toLowerCase() === 'android') {
            //开始构建的时候，先发个事件
            trackBuildEvent();

            await _handleAndroid(options);
        } else if (options.actualPlatform.toLowerCase() === "ios") {
          
        }
        cb && cb();
    } catch (e) {
        cb && cb(e);
    }
}

function trackBuildEvent() {
    Editor.Metrics.trackEvent({
        category: 'Google Play',
        action: 'GooglePlay Billing',
        label: 'Build'
    });
}

module.exports = {
    load() {
        Editor.Builder.on('before-change-files', handleEvent);
    },

    unload() {
        Editor.Builder.removeListener('before-change-files', handleEvent);
    },

    messages: {}
};
